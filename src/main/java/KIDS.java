/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class KIDS {
    public String name;
    public int age;
    public KIDS(String name,int age){
        this.name=name;
        this.age=age;
    }
    //Overloading มีชื่อเหมือนกันหรือพฤติกรรมเหมือนกันแต่มีคำสั่งต่างกัน
    public void Children(String name,int age){
        System.out.println(name+" Child");
    }
    //Overloading มีชื่อเหมือนกันหรือพฤติกรรมเหมือนกันแต่มีคำสั่งต่างกัน
    public void Children(String name){
        System.out.println(name+" Baby");
    }
    
}
